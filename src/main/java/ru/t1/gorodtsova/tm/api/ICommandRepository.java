package ru.t1.gorodtsova.tm.api;

import ru.t1.gorodtsova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
